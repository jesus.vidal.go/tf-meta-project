### ASG to allow restore in case of a failure
module "ec2-asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 4.9"
  name    = "${var.environment}-bastion"

  lc_name   = "${var.environment}-bastion-lc"
  use_lc    = true
  create_lc = true

  image_id      = "ami-0fb653ca2d3203ac1"
  instance_type = "t2.small"
  security_groups             = [module.launch_sg.security_group_id]
  associate_public_ip_address = true
  iam_instance_profile_name   = aws_iam_instance_profile.ec2.name


  root_block_device = [
    {
      volume_size           = "10"
      volume_type           = "gp2"
      delete_on_termination = true
    },
  ]

  # Auto scaling group
  vpc_zone_identifier       = [module.vpc.private_subnets[0]]
  health_check_type         = "EC2"
  min_size                  = 1
  max_size                  = 1
  desired_capacity          = 1
  wait_for_capacity_timeout = 0
  key_name                  = var.public_key_name

  tags = [
    {
      key                 = "project"
      value               = "metadata"
      propagate_at_launch = true
    }
  ]
}
