module "key-pair" {
  source  = "terraform-aws-modules/key-pair/aws"
  version = "1.0.0"

  key_name   = var.public_key_name
  public_key = var.public_key
}
