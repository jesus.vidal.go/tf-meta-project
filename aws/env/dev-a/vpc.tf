locals {
  project_name = "metadata-${var.environment}"
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.7.0"

  name = "${local.project_name}-demo"
  cidr = var.vpc_cidr

  azs             = ["${local.region}a", "${local.region}b"]
  private_subnets = var.vpc_private_subnets
  public_subnets  = var.vpc_public_subnets

  enable_nat_gateway = true

  tags = {
    "terraform"                                   = "true"
    "environment"                                 = var.environment
    "product_line"                                = "apps"
    "environment"                                 = "development"
    "origin"                                      = "https://gitlab.com/jesus.vidal.go/tf-meta-project"
    "confidentiality"                             = "private"
    "compliance"                                  = "none"
  }

  private_subnet_tags = {
    "Access"                                                 = "Private"
  }

  public_subnet_tags = {
    "Access"                                                 = "Public"
  }

}
