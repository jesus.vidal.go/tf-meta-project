variable "environment" {
  type    = string
  default = "dev"
}
# VPC
variable "vpc_cidr" {
  type    = string
  default = "10.100.0.0/16"
}
variable "vpc_private_subnets" {
  type    = list(any)
  default = ["10.100.100.0/24", "10.100.100.0/24"]
}
variable "vpc_public_subnets" {
  type    = list(any)
  default = ["10.100.200.0/24", "10.100.201.0/24"]
}
# key
variable "public_key_name" {
  type        = string
  default     = "test"
  description = "ssh key name"
}
variable "public_key" {
  type        = string
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDO7QVy81AoFe5eXnwk7xSwe0eiRAOswxpVTsXXZd8zV7Y1RbIWoksnjSJtjyqVlc9QXLijRF+wyYqydGhNQ7Fs+fbi2tlwArrhRGWXefL9XR8lDAWUHlKbPiDjMuDxqF0/yWd4bmieD7fC7p6pqWmWLZE2h9ew2x7LNC8aeNpzqNFk2aIEWjhwJ/+y5QTP9xWpQKWFBbcGPOB+nneK1XMayxBoaa03kM+74sX+3B84x5gnwLTQvAB8vOqATd8UMtbp0fDkA2tHAclCr8fRCQPWWz/5AZ5Gjn6KeQ+ARU7+Au1J6Tx2FULU+ic8z3Dml0oUHvG39ig7pUXlze2q1+Hp jesusgomez@MacBook-Pro-de-Admin-2.local"
  description = "ssh public key"
}
# list of policies to attach to ec2 box role 
variable "launch_iam_policy_arn" {
  description = "IAM Policy to be attached to role"
  type        = set(string)
  default = [
    "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore",
    "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  ]
}
