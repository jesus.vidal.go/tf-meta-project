resource "aws_iam_instance_profile" "ec2" {
  name = "ec2-profile"
  role = aws_iam_role.ec2_demo.name
}

resource "aws_iam_role_policy_attachment" "ec2_role_policy_attachment" {
  for_each = var.launch_iam_policy_arn

  role       = aws_iam_role.ec2_demo.name
  policy_arn = each.value
}

resource "aws_iam_role" "ec2_demo" {
  name = "ec2-launch"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}
