# Organization
resource "aws_organizations_organization" "org" {
  aws_service_access_principals = [
    "cloudtrail.amazonaws.com",
    "config.amazonaws.com",
    "sso.amazonaws.com"
  ]

  feature_set = "ALL"
}

resource "aws_organizations_organizational_unit" "workload" {
  name      = "workload"
  parent_id = aws_organizations_organization.this.roots[0].id
}

resource "aws_organizations_organizational_unit" "dev" {
  name      = "demo"
  parent_id = aws_organizations_organizational_unit.workload.id

  depends_on = [
    aws_organizations_organizational_unit.workload
  ]
}
# Account A
resource "aws_organizations_account" "dev" {
  name  = "metadata-account-a"
  email = "jesus.vidal.go@gmail.com"

  iam_user_access_to_billing = "ALLOW"

  tags = {
    Name  = "metadata-dev"
    Owner = "Jesus V"
  }

  parent_id = aws_organizations_organizational_unit.dev.id
}
# Account B
resource "aws_organizations_account" "dev" {
  name  = "metadata-account-b"
  email = "jesus.vidal.go@gmail.com"

  iam_user_access_to_billing = "ALLOW"

  tags = {
    Name  = "metadata-dev"
    Owner = "Jesus V"
  }

  parent_id = aws_organizations_organizational_unit.dev.id
}
