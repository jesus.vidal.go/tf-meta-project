#Backend
variable "dev_dynamodb_table_name" {
  type    = string
  default = "metadata-dev-state"

}

variable "dev_s3_bucket_name" {
  type    = string
  default = "metadata-dev-state"
}
